//
//  TTAppDelegate.h
//  TTTest
//
//  Created by Diego Peinador on 22/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
