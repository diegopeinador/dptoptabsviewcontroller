//
//  TTSimpleViewController.m
//  TTTest
//
//  Created by Diego Peinador on 21/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import "TTSimpleViewController.h"
#import "DPTopTabsViewController.h"

@interface TTSimpleViewController (){
    UILabel *_label;
    UITextField *_textField;
    UIButton *_save, *_changeIcon;
    NSUInteger imageNum;
}

@end

@implementation TTSimpleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        imageNum = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.bounds.size.width-20, 21)];
    _label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _label.text = @"Change title:";
    _label.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_label];
    
    self.view.backgroundColor = _fondo;
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 41, 100, 31)];
    _textField.text = self.toptapItem.title;
    _textField.borderStyle=UITextBorderStyleRoundedRect;
    [self.view addSubview:_textField];
    
    _save = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _save.frame = CGRectMake(120, 38, 80, 37);
    [_save setTitle:@"Change" forState:UIControlStateNormal];
    [_save addTarget:self action:@selector(changeTitle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_save];
    
    _changeIcon = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _changeIcon.frame = CGRectMake(10, 82, 190, 37);
    [_changeIcon setTitle:@"Change Icon" forState:UIControlStateNormal];
    [_changeIcon addTarget:self action:@selector(changeIcon:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_changeIcon];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    _label=nil;
    _textField=nil;
    _save=nil;
    _changeIcon=nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)changeTitle:(id)sender{
    self.toptapItem.title = _textField.text;
}

-(void)changeIcon:(id)sender{
    imageNum = ++imageNum%10;
    self.toptapItem.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",imageNum]];
}

@end
