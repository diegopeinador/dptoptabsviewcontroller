//
//  TTSimpleViewController.h
//  TTTest
//
//  Created by Diego Peinador on 21/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSimpleViewController : UIViewController

@property(nonatomic,strong) UIColor *fondo;

@end
